package co.edu.uniajc.coopfamiliar.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "credits")
public class CreditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "value")
    private Double value;

    @Column(name = "due_day")
    private int dueDay;

    @Column(name = "refund_date")
    private Date refundDate;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "description")
    private String description;

    @Column(name = "approve")
    private String approve;

    @Column(name = "associate_id")
    private int associateId;

    @OneToOne
    @JoinColumn(name = "associate_id", insertable = false, updatable = false)
    private AssociateModel associate;

}
