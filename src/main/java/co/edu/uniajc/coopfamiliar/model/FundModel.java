package co.edu.uniajc.coopfamiliar.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "funds")
public class FundModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "total")
    private Double total;

    @Column(name = "value")
    private Double value;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "date_creation")
    private Date dateCreation;

    @Column(name = "contribution_id")
    private int contributionId;

    @OneToOne
    @JoinColumn(name = "contribution_id", insertable = false, updatable = false)
    private ContributionModel contribution;

}
