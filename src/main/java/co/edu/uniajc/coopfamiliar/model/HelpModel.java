package co.edu.uniajc.coopfamiliar.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "helps")
public class HelpModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "value")
    private Double value;

    @Column(name = "date_creation")
    private Date dateCreation;

    @Column(name = "type")
    private String type;

    @Column(name = "associate_id")
    private int associateId;

    @Column(name = "fund_id")
    private int fundId;

    @OneToOne
    @JoinColumn(name = "associate_id", insertable = false, updatable = false)
    private AssociateModel associate;

    @OneToOne
    @JoinColumn(name = "fund_id", insertable = false, updatable = false)
    private FundModel fund;

}
