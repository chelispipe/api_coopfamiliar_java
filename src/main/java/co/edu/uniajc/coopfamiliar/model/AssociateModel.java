package co.edu.uniajc.coopfamiliar.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "associates")
public class AssociateModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "identification_number")
    private @NonNull String identificationNumber;

    @Column(name = "first_name")
    private @NonNull String firstName;

    @Column(name = "last_name")
    private @NonNull String lastName;

    @Column(name = "status")
    private @NonNull Boolean status;

    @Column(name = "date_in")
    private @NonNull Date dateIn;

    @ManyToMany
    @JoinTable(name = "roles_associates",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private List<RolModel> roles;

}