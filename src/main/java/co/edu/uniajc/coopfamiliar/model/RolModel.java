package co.edu.uniajc.coopfamiliar.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class RolModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private @NonNull String name;

    @Column(name = "description")
    private @NonNull String description;

    @Column(name = "status")
    private @NonNull Boolean status;

}
