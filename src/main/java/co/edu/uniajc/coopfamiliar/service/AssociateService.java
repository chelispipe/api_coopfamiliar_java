package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.AssociateModel;
import co.edu.uniajc.coopfamiliar.repository.AssociateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssociateService {

    private final AssociateRepository associateRepository;

    @Autowired
    public AssociateService(AssociateRepository associateRepository) {
        this.associateRepository = associateRepository;
    }

    public AssociateModel createAssociate(AssociateModel associateModel) {
        return associateRepository.save(associateModel);
    }

    public AssociateModel updateAssociate(AssociateModel associateModel) {
        return associateRepository.save(associateModel);
    }

    public void deleteAssociate(Integer id) {
        associateRepository.deleteById(id);
    }

    public List<AssociateModel> findAllAssociate() {
        return associateRepository.findAll();
    }

    public List<AssociateModel> findAllByName(String name) {
        return associateRepository.findByFirstNameAndLastName(name);
    }

    public List<AssociateModel> findAllByStatus(Boolean status) {
        return associateRepository.findByStatus(status);
    }

    public Optional<AssociateModel> findById(int id) {
        return associateRepository.findById(id);
    }

    public List<AssociateModel> findByIdentificationNumber(String identification) {
        return associateRepository.findAllByIdentificationNumber(identification);
    }

}
