package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.RolModel;
import co.edu.uniajc.coopfamiliar.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RolService {

    private final RolRepository rolRepository;

    @Autowired
    public RolService(RolRepository rolRepository) {
        this.rolRepository = rolRepository;
    }

    public RolModel createRol(RolModel rolModel) {
        return rolRepository.save(rolModel);
    }

    public RolModel updateRol(RolModel rolModel) {
        return rolRepository.save(rolModel);
    }

    public void deleteRol(Integer id) {
        rolRepository.deleteById(id);
    }

    public List<RolModel> findAllRol() {
        return rolRepository.findAll();
    }

    public List<RolModel> findAllByName(String name) {
        return rolRepository.findAllByName(name);
    }

    public List<RolModel> findAllByStatus(Boolean status) {
        return rolRepository.findByStatus(status);
    }

    public Optional<RolModel> findById(int id) {
        return rolRepository.findById(id);
    }

}
