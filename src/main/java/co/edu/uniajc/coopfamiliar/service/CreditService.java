package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.CreditModel;
import co.edu.uniajc.coopfamiliar.repository.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CreditService {

    private final CreditRepository creditRepository;

    @Autowired
    public CreditService(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    public CreditModel createCredit(CreditModel creditModel) {
        return creditRepository.save(creditModel);
    }

    public CreditModel updateCredit(CreditModel creditModel) {
        return creditRepository.save(creditModel);
    }

    public void deleteCredit(Integer id) {
        creditRepository.deleteById(id);
    }

    public List<CreditModel> findAllCredit() {
        return creditRepository.findAll();
    }

    public Optional<CreditModel> findById(int id) {
        return creditRepository.findById(id);
    }

    public List<CreditModel> findByAssociateId(int id) {
        return creditRepository.findByAssociate_Id(id);
    }

    public List<CreditModel> findByStatus(Boolean state) {
        return creditRepository.findByStatus(state);
    }

}
