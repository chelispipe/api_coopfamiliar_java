package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.ContributionModel;
import co.edu.uniajc.coopfamiliar.repository.ContributionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ContributionService {

    private final ContributionRepository contributionRepository;

    @Autowired
    public ContributionService(ContributionRepository contributionRepository) {
        this.contributionRepository = contributionRepository;
    }

    public ContributionModel createContribution(ContributionModel contributionModel) {
        return contributionRepository.save(contributionModel);
    }

    public ContributionModel updateContribution(ContributionModel contributionModel) {
        return contributionRepository.save(contributionModel);
    }

    public void deleteContribution(Integer id) {
        contributionRepository.deleteById(id);
    }

    public List<ContributionModel> findAllContribution() {
        return contributionRepository.findAll();
    }

    public Optional<ContributionModel> findById(int id) {
        return contributionRepository.findById(id);
    }

    public List<ContributionModel> findBetweenDates(String dateFrom, String dateTo) {
        return contributionRepository.findDataByDates(dateFrom, dateTo);
    }

}
