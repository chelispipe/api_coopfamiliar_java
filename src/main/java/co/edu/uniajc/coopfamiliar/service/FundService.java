package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.FundModel;
import co.edu.uniajc.coopfamiliar.repository.FundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FundService {

    private final FundRepository fundRepository;

    @Autowired
    public FundService(FundRepository fundRepository) {
        this.fundRepository = fundRepository;
    }

    public FundModel createFund(FundModel fundModel) {
        return fundRepository.save(fundModel);
    }

    public FundModel updateFund(FundModel fundModel) {
        return fundRepository.save(fundModel);
    }

    public void deleteFund(Integer id) {
        fundRepository.deleteById(id);
    }

    public List<FundModel> findAllFund() {
        return fundRepository.findAll();
    }

    public List<FundModel> findByName(String name) {
        return fundRepository.findByName(name);
    }

    public Optional<FundModel> findById(int id) {
        return fundRepository.findById(id);
    }

}
