package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.HelpModel;
import co.edu.uniajc.coopfamiliar.repository.HelpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HelpService {

    private final HelpRepository helpRepository;

    @Autowired
    public HelpService(HelpRepository helpRepository) {
        this.helpRepository = helpRepository;
    }

    public HelpModel createHelp(HelpModel helpModel) {
        return helpRepository.save(helpModel);
    }

    public HelpModel updateHelp(HelpModel helpModel) {
        return helpRepository.save(helpModel);
    }

    public void deleteHelp(Integer id) {
        helpRepository.deleteById(id);
    }

    public List<HelpModel> findAllHelp() {
        return helpRepository.findAll();
    }

    public List<HelpModel> findByType(String name) {
        return helpRepository.findByType(name);
    }

    public Optional<HelpModel> findById(int id) {
        return helpRepository.findById(id);
    }

}
