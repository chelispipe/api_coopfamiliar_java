package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.FundModel;
import co.edu.uniajc.coopfamiliar.service.FundService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/fund")
@Api("Funds")
public class FundController {

    private final ResponseUtil responseUtil = new ResponseUtil();
    private final FundService fundService;

    @Autowired
    public FundController(FundService fundService) {
        this.fundService = fundService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Fund", response = FundModel.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> saveFund(@RequestBody FundModel fundModel) {
        return new ResponseEntity<Object>(responseUtil.saveSuccess(fundService.createFund(fundModel)),
                HttpStatus.CREATED);
    }

    @GetMapping("/all")
    @ApiOperation("Get all Funds")
    @ApiResponse(code = 200, message = "ok")
    public ResponseEntity<List<FundModel>> getAll() {
        return new ResponseEntity<>(fundService.findAllFund(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get Fund by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Fund not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findById(@ApiParam(value = "The id of the Fund", required = true, example = "9")
             @PathVariable("id") int id) {
        return fundService.findById(id)
                .map(record -> new ResponseEntity<Object>(record, HttpStatus.OK))
                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND));
    }

    @GetMapping("/name/{name}")
    @ApiOperation("Get Funds by name")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Fund not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findByName(@ApiParam(value = "The name of the Fund", required = true, example = "Admin")
               @PathVariable("name") String name) {
        return new ResponseEntity<>(fundService.findByName(name), HttpStatus.OK);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value="Update Fund", response = FundModel.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> updateFund(@RequestBody FundModel fundModel) {
        return new ResponseEntity<Object>(responseUtil.updateSuccess(fundService.updateFund(fundModel)), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Delete Fund by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Fund not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    deleteFund(@ApiParam(value = "The id of the Fund", required = true, example = "9")
              @PathVariable("id") int id) {
        Optional<FundModel> fund = fundService.findById(id);
        if (fund.isPresent()) {
            fundService.deleteFund(id);
            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND);
        }
    }
}
