package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.CreditModel;
import co.edu.uniajc.coopfamiliar.service.CreditService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/credit")
@Api("Credits")
public class CreditController {

    private final ResponseUtil responseUtil = new ResponseUtil();
    private final CreditService creditService;

    @Autowired
    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Credit", response = CreditModel.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> saveCredit(@RequestBody CreditModel creditModel) {
        return new ResponseEntity<Object>(responseUtil.saveSuccess(creditService.createCredit(creditModel)),
                HttpStatus.CREATED);
    }

    @GetMapping("/all")
    @ApiOperation("Get all Credits")
    @ApiResponse(code = 200, message = "ok")
    public ResponseEntity<List<CreditModel>> getAll() {
        return new ResponseEntity<>(creditService.findAllCredit(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get Credit by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Credit not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findById(@ApiParam(value = "The id of the Credit", required = true, example = "9")
             @PathVariable("id") int id) {
        return creditService.findById(id)
                .map(record -> new ResponseEntity<Object>(record, HttpStatus.OK))
                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND));
    }

    @GetMapping("/associate/{id}")
    @ApiOperation("Get Credits by AssociateId")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Credit not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findByAssociateId(@ApiParam(value = "The AssociateId of the Credit", required = true, example = "1")
               @PathVariable("id") int id) {
        return new ResponseEntity<>(creditService.findByAssociateId(id), HttpStatus.OK);
    }

    @GetMapping("/status/{status}")
    @ApiOperation("Get Credits by status")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Credit not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findByStatus(@ApiParam(value = "The status of the Credit", required = true, example = "true", type = "Boolean")
                 @PathVariable("status") Boolean status) {
        return new ResponseEntity<>(creditService.findByStatus(status), HttpStatus.OK);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value="Update Credit", response = CreditModel.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> updateCredit(@RequestBody CreditModel creditModel) {
        return new ResponseEntity<Object>(responseUtil.updateSuccess(creditService.updateCredit(creditModel)),
                HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Delete Credit by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Credit not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    deleteCredit(@ApiParam(value = "The id of the Credit", required = true, example = "9")
              @PathVariable("id") int id) {
        Optional<CreditModel> credit = creditService.findById(id);
        if (credit.isPresent()) {
            creditService.deleteCredit(id);
            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND);
        }
    }

}
