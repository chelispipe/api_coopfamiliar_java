package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.AssociateModel;
import co.edu.uniajc.coopfamiliar.service.AssociateService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/associate")
@Api("Associates")
public class AssociateController {

    private final AssociateService associateService;
    private final ResponseUtil responseUtil = new ResponseUtil();

    @Autowired
    public AssociateController(AssociateService associateService) {
        this.associateService = associateService;
    }

//    @PostMapping(path = "/save")
//    @ApiOperation(value="Insert Associate", response = AssociateModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 201, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> saveAssociate(@RequestBody AssociateModel associateModel) {
//        return new ResponseEntity<Object>(responseUtil.saveSuccess(associateService.createAssociate(associateModel)),
//                HttpStatus.CREATED);
//    }
//
//    @GetMapping("/all")
//    @ApiOperation("Get all associates")
//    @ApiResponse(code = 200, message = "ok")
//    public ResponseEntity<List<AssociateModel>> getAll() {
//        return new ResponseEntity<>(associateService.findAllAssociate(), HttpStatus.OK);
//    }
//
//    @GetMapping("/{id}")
//    @ApiOperation("Get associate by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Associate not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findById(@ApiParam(value = "The id of the associate", required = true, example = "9")
//             @PathVariable("id") int associateId) {
//        return associateService.findById(associateId)
//                .map(associate -> new ResponseEntity<Object>(associate, HttpStatus.OK))
//                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(associateId), HttpStatus.NOT_FOUND));
//    }
//
//    @GetMapping("/name/{name}")
//    @ApiOperation("Get associates by name")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "associates not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByName(@ApiParam(value = "The name of the associates", required = true, example = "Felipe")
//               @PathVariable("name") String name) {
//        return new ResponseEntity<>(associateService.findAllByName(name), HttpStatus.OK);
//    }
//
//    @GetMapping("/identification/{identification}")
//    @ApiOperation("Get associates by identification")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "associates not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByIdentification(@ApiParam(value = "The identification of the associates", required = true,
//            example = "123456789")
//               @PathVariable("identification") String identification) {
//        return new ResponseEntity<>(associateService.findByIdentificationNumber(identification), HttpStatus.OK);
//    }
//
//    @GetMapping("/status/{status}")
//    @ApiOperation("Get associates by status")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "associates not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByStatus(@ApiParam(value = "The status of the associates", required = true, example = "true", type = "Boolean")
//                 @PathVariable("status") Boolean status) {
//        return new ResponseEntity<>(associateService.findAllByStatus(status), HttpStatus.OK);
//    }
//
//    @PutMapping(path = "/update")
//    @ApiOperation(value="Update associates", response = AssociateModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> updateAssociate(@RequestBody AssociateModel associateModel) {
//        return new ResponseEntity<Object>(responseUtil.updateSuccess(associateService.updateAssociate(associateModel)),
//                HttpStatus.OK);
//    }
//
//    @DeleteMapping("/delete/{id}")
//    @ApiOperation("Delete associates by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Associate not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    deleteAssociate(@ApiParam(value = "The id of the associate", required = true, example = "9")
//              @PathVariable("id") int associateId) {
//        Optional<AssociateModel> associate = associateService.findById(associateId);
//        if (associate.isPresent()) {
//            associateService.deleteAssociate(associateId);
//            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<Object>(responseUtil.notFoundById(associateId), HttpStatus.NOT_FOUND);
//        }
//    }

}
