package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.HelpModel;
import co.edu.uniajc.coopfamiliar.service.HelpService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/help")
@Api("Helps")
public class HelpController {

    private final ResponseUtil responseUtil = new ResponseUtil();
    private final HelpService helpService;

    @Autowired
    public HelpController(HelpService helpService) {
        this.helpService = helpService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Help", response = HelpModel.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> saveHelp(@RequestBody HelpModel helpModel) {
        return new ResponseEntity<Object>(responseUtil.saveSuccess(helpService.createHelp(helpModel)),
                HttpStatus.CREATED);
    }

    @GetMapping("/all")
    @ApiOperation("Get all Helps")
    @ApiResponse(code = 200, message = "ok")
    public ResponseEntity<List<HelpModel>> getAll() {
        return new ResponseEntity<>(helpService.findAllHelp(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get Help by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Help not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findById(@ApiParam(value = "The id of the Help", required = true, example = "9")
             @PathVariable("id") int id) {
        return helpService.findById(id)
                .map(record -> new ResponseEntity<Object>(record, HttpStatus.OK))
                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND));
    }

    @GetMapping("/type/{type}")
    @ApiOperation("Get Helps by type")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Help not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    findByType(@ApiParam(value = "The type of the Help", required = true, example = "Admin")
               @PathVariable("type") String type) {
        return new ResponseEntity<>(helpService.findByType(type), HttpStatus.OK);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value="Update Help", response = HelpModel.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> updateHelp(@RequestBody HelpModel helpModel) {
        return new ResponseEntity<Object>(responseUtil.updateSuccess(helpService.updateHelp(helpModel)), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Delete Help by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 404, message = "Help not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object>
    deleteHelp(@ApiParam(value = "The id of the Help", required = true, example = "9")
              @PathVariable("id") int id) {
        Optional<HelpModel> help = helpService.findById(id);
        if (help.isPresent()) {
            helpService.deleteHelp(id);
            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(responseUtil.notFoundById(id), HttpStatus.NOT_FOUND);
        }
    }
}
