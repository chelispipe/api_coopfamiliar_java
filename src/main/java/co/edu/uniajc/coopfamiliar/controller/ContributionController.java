package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.ContributionModel;
import co.edu.uniajc.coopfamiliar.service.ContributionService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/contribution")
@Api("Contributions")
public class ContributionController {

    private final ResponseUtil responseUtil = new ResponseUtil();
    private final ContributionService contributionService;

    @Autowired
    public ContributionController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

//    @PostMapping(path = "/save")
//    @ApiOperation(value="Insert Contribution", response = ContributionModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 201, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> saveContribution(@RequestBody ContributionModel contributionModel) {
//        return new ResponseEntity<Object>(responseUtil.saveSuccess(
//                contributionService.createContribution(contributionModel)), HttpStatus.CREATED);
//    }
//
//    @GetMapping("/all")
//    @ApiOperation("Get all Contributions")
//    @ApiResponse(code = 200, message = "ok")
//    public ResponseEntity<List<ContributionModel>> getAll() {
//        return new ResponseEntity<>(contributionService.findAllContribution(), HttpStatus.OK);
//    }
//
//    @GetMapping("/{id}")
//    @ApiOperation("Get Contribution by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Contribution not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findById(@ApiParam(value = "The id of the Contribution", required = true, example = "9")
//             @PathVariable("id") int contributionId) {
//        return contributionService.findById(contributionId)
//                .map(contribution -> new ResponseEntity<Object>(contribution, HttpStatus.OK))
//                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(contributionId), HttpStatus.NOT_FOUND));
//    }
//
//    @GetMapping("/search/dates")
//    @ApiOperation("Get Contribution by status")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Contribution not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByStatus(@ApiParam(value = "The status of the Contribution", required = true, example = "true", type = "Boolean")
//                 @RequestParam(value="dateFrom", required=true) String dateFrom,
//                 @RequestParam(value="dateTo", required=true) String dateTo) {
//        return new ResponseEntity<>(contributionService.findBetweenDates(dateFrom, dateTo), HttpStatus.OK);
//    }
//
//    @PutMapping(path = "/update")
//    @ApiOperation(value="Update Contribution", response = ContributionModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> updateContribution(@RequestBody ContributionModel contributionModel) {
//        return new ResponseEntity<Object>(responseUtil.updateSuccess(
//                contributionService.updateContribution(contributionModel)), HttpStatus.OK);
//    }
//
//    @DeleteMapping("/delete/{id}")
//    @ApiOperation("Delete Contribution by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Contribution not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    deleteContribution(@ApiParam(value = "The id of the Contribution", required = true, example = "9")
//              @PathVariable("id") int contributionId) {
//        Optional<ContributionModel> contribution = contributionService.findById(contributionId);
//        if (contribution.isPresent()) {
//            contributionService.deleteContribution(contributionId);
//            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<Object>(responseUtil.notFoundById(contributionId), HttpStatus.NOT_FOUND);
//        }
//    }
}
