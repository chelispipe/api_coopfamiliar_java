package co.edu.uniajc.coopfamiliar.controller;

import co.edu.uniajc.coopfamiliar.model.RolModel;
import co.edu.uniajc.coopfamiliar.service.RolService;
import co.edu.uniajc.coopfamiliar.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("/roles")
@Api("Roles")
public class RolController {

    private final RolService rolService;
    private final ResponseUtil responseUtil = new ResponseUtil();

    @Autowired
    public RolController(RolService rolService) {
        this.rolService = rolService;
    }

//    @PostMapping(path = "/save")
//    @ApiOperation(value="Insert Rol", response = RolModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 201, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> saveRol(@RequestBody RolModel rolModel) {
//        return new ResponseEntity<Object>(responseUtil.saveSuccess(rolService.createRol(rolModel)), HttpStatus.CREATED);
//    }
//
//    @GetMapping("/all")
//    @ApiOperation("Get all roles")
//    @ApiResponse(code = 200, message = "ok")
//    public ResponseEntity<List<RolModel>> getAll() {
//        return new ResponseEntity<>(rolService.findAllRol(), HttpStatus.OK);
//    }
//
//    @GetMapping("/{id}")
//    @ApiOperation("Get rol by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Rol not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findById(@ApiParam(value = "The id of the rol", required = true, example = "9")
//             @PathVariable("id") int rolId) {
//        return rolService.findById(rolId)
//                .map(rol -> new ResponseEntity<Object>(rol, HttpStatus.OK))
//                .orElse(new ResponseEntity<Object>(responseUtil.notFoundById(rolId), HttpStatus.NOT_FOUND));
//    }
//
//    @GetMapping("/name/{name}")
//    @ApiOperation("Get roles by name")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Rol not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByName(@ApiParam(value = "The name of the rol", required = true, example = "Admin")
//               @PathVariable("name") String name) {
//        return new ResponseEntity<>(rolService.findAllByName(name), HttpStatus.OK);
//    }
//
//    @GetMapping("/status/{status}")
//    @ApiOperation("Get roles by status")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Rol not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    findByStatus(@ApiParam(value = "The status of the rol", required = true, example = "true", type = "Boolean")
//               @PathVariable("status") Boolean status) {
//        return new ResponseEntity<>(rolService.findAllByStatus(status), HttpStatus.OK);
//    }
//
//    @PutMapping(path = "/update")
//    @ApiOperation(value="Update Rol", response = RolModel.class)
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 400, message = "Something went wrong"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object> updateRol(@RequestBody RolModel rolModel) {
//        return new ResponseEntity<Object>(responseUtil.updateSuccess(rolService.updateRol(rolModel)), HttpStatus.OK);
//    }
//
//    @DeleteMapping("/delete/{id}")
//    @ApiOperation("Delete rol by id")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok"),
//            @ApiResponse(code = 404, message = "Rol not found"),
//            @ApiResponse(code = 500, message = "Internal server error")
//    })
//    public ResponseEntity<Object>
//    deleteRol(@ApiParam(value = "The id of the rol", required = true, example = "9")
//              @PathVariable("id") int rolId) {
//        Optional<RolModel> rol = rolService.findById(rolId);
//        if (rol.isPresent()) {
//            rolService.deleteRol(rolId);
//            return new ResponseEntity<Object>(responseUtil.deleteSuccess(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<Object>(responseUtil.notFoundById(rolId), HttpStatus.NOT_FOUND);
//        }
//    }

}
