package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.AssociateModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssociateRepository extends JpaRepository<AssociateModel, Integer> {
    List<AssociateModel> findAllByIdentificationNumber(String identification);
    AssociateModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM associates " +
            "WHERE status = :state"
    )
    List<AssociateModel> findByStatus(@Param(value = "state") Boolean state);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM associates " +
            "WHERE concat(first_name,' ',last_name) like %:name%"
    )
    List<AssociateModel> findByFirstNameAndLastName(@Param(value = "name") String name);
}
