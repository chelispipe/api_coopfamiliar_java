package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.HelpModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HelpRepository extends JpaRepository<HelpModel, Integer> {

    HelpModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM helps " +
            "WHERE type like %:name%"
    )
    List<HelpModel> findByType(@Param(value = "name") String name);
}
