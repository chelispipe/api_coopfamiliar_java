package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.CreditModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CreditRepository extends JpaRepository<CreditModel, Integer> {

    CreditModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM credits " +
            "WHERE associates_id = :id"
    )
    List<CreditModel> findByAssociate_Id(@Param(value = "id") int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM credits " +
            "WHERE status = :state"
    )
    List<CreditModel> findByStatus(@Param(value = "state") Boolean state);

}
