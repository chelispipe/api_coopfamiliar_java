package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.ContributionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ContributionRepository extends JpaRepository<ContributionModel, Integer> {

    ContributionModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM contributions " +
            "WHERE refund_date between :dateFrom and :dateTo"
    )
    List<ContributionModel> findDataByDates(@Param(value = "dateFrom") String dateFrom,
                                                @Param(value = "dateTo") String dateTo);
}
