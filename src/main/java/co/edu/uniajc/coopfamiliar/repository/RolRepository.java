package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.RolModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolRepository  extends JpaRepository<RolModel, Integer> {

    List<RolModel> findAllByName(String name);
    RolModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM roles " +
            "WHERE status = :state"
    )
    List<RolModel> findByStatus(@Param(value = "state") Boolean state);

}
