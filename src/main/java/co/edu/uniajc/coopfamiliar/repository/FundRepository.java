package co.edu.uniajc.coopfamiliar.repository;

import co.edu.uniajc.coopfamiliar.model.FundModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FundRepository extends JpaRepository<FundModel, Integer> {

    FundModel getById(int id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM funds " +
            "WHERE name like %:name%"
    )
    List<FundModel> findByName(@Param(value = "name") String name);

}
