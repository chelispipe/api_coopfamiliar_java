package co.edu.uniajc.coopfamiliar.utils;

import java.util.HashMap;

public class ResponseUtil {

    public ResponseUtil() {
    }

    public Object notFound() {
        HashMap<String, Object> response = new HashMap<>();
        response.put("type", "NOT_FOUND");
        response.put("statusCode", 404);
        response.put("message", "Recurso no entrado");
        return response;
    }

    public Object notFoundById(int id) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("type", "NOT_FOUND");
        response.put("statusCode", 404);
        response.put("message", "No existe un registro con el ID." + id);
        return response;
    }

    public Object saveSuccess(Object model) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("ok", true);
        response.put("statusCode", 201);
        response.put("message", "Registro almacenado correctamente");
        response.put("data", model);
        return response;
    }

    public Object updateSuccess(Object model) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("ok", true);
        response.put("statusCode", 200);
        response.put("message", "Registro modificado correctamente");
        response.put("data", model);
        return response;
    }

    public Object deleteSuccess() {
        HashMap<String, Object> response = new HashMap<>();
        response.put("ok", true);
        response.put("statusCode", 200);
        response.put("message", "Registro eliminado correctamente");
        return response;
    }

}
