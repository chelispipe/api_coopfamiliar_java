CREATE TABLE "roles"
(
    "id"     SERIAL PRIMARY KEY,
    "name"   varchar,
    "status" boolean
);

CREATE TABLE "associates"
(
    "id"                    SERIAL PRIMARY KEY,
    "first_name"            varchar,
    "last_name"             varchar,
    "identification_number" varchar,
    "status"                boolean,
    "date_in"               date
);

CREATE TABLE "roles_associates"
(
    "id"           SERIAL PRIMARY KEY,
    "rol_id"       int,
    "associate_id" int
);

CREATE TABLE "contributions"
(
    "id"           SERIAL PRIMARY KEY,
    "value"        decimal,
    "due_day"      int,
    "refund_date"  date,
    "status"       boolean,
    "description"  varchar,
    "associate_id" int
);

CREATE TABLE "credits"
(
    "id"           SERIAL PRIMARY KEY,
    "value"        decimal,
    "due_day"      int,
    "refund_date"  date,
    "status"       boolean,
    "description"  varchar,
    "approve"      varchar,
    "associate_id" int
);

CREATE TABLE "funds"
(
    "id"              SERIAL PRIMARY KEY,
    "name"            varchar,
    "total"           decimal,
    "value"           decimal,
    "status"          boolean,
    "date_creation"   date,
    "contribution_id" int
);

CREATE TABLE "helps"
(
    "id"            SERIAL PRIMARY KEY,
    "value"         decimal,
    "date_creation" date,
    "type"          varchar,
    "associate_id"  int,
    "fund_id"       int
);

ALTER TABLE "roles_associates"
    ADD FOREIGN KEY ("rol_id") REFERENCES "roles" ("id");

ALTER TABLE "roles_associates"
    ADD FOREIGN KEY ("associate_id") REFERENCES "associates" ("id");

ALTER TABLE "contributions"
    ADD FOREIGN KEY ("associate_id") REFERENCES "associates" ("id");

ALTER TABLE "credits"
    ADD FOREIGN KEY ("associate_id") REFERENCES "associates" ("id");

ALTER TABLE "funds"
    ADD FOREIGN KEY ("contribution_id") REFERENCES "contributions" ("id");

ALTER TABLE "helps"
    ADD FOREIGN KEY ("associate_id") REFERENCES "associates" ("id");

ALTER TABLE "helps"
    ADD FOREIGN KEY ("fund_id") REFERENCES "funds" ("id");
