package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.CreditModel;
import co.edu.uniajc.coopfamiliar.repository.CreditRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CreditServiceTest {

    @Mock
    private CreditRepository creditRepository;

    @InjectMocks
    private CreditService creditService;

    private CreditModel credit;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        String dateStr = "2021-09-10";
        Date refundDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);

        credit = new CreditModel();
        credit.setId(1);
        credit.setAssociateId(1);
        credit.setDescription("Crédito Vivienda");
        credit.setDueDay(29);
        credit.setStatus(true);
        credit.setApprove("Aprobado por: Carlos, Felipe");
        credit.setValue(2355689.0);
        credit.setRefundDate(refundDate);

    }

    @Test
    void createCredit() {
        Mockito.when(creditRepository.save(credit)).thenReturn(credit);
        Assertions.assertNotNull(creditService.createCredit(credit));
        Assertions.assertEquals(credit, creditService.createCredit(credit));
    }

    @Test
    void updateCredit() {
        Mockito.when(creditRepository.save(credit)).thenReturn(credit);
        Assertions.assertNotNull(creditService.updateCredit(credit));
        Assertions.assertEquals(credit, creditService.updateCredit(credit));
    }

    @Test
    void deleteCredit() {
        int id = 1;
        creditService.deleteCredit(id);
        Mockito.verify(creditRepository, Mockito.times(id)).deleteById(id);
    }

    @Test
    void findAllCredit() {
        Mockito.when(creditRepository.findAll()).thenReturn(List.of(credit));
        Assertions.assertNotNull(creditService.findAllCredit());
        Assertions.assertTrue(List.of(credit).size() > 0);
        Assertions.assertEquals(List.of(credit).get(0).getDescription(), "Crédito Vivienda");
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(creditRepository.findById(id)).thenReturn(Optional.of(credit));
        Assertions.assertNotNull(creditService.findById(id));
        Assertions.assertEquals(Optional.of(credit).get().getId(), id);
        Assertions.assertEquals(Optional.of(credit).get().getDescription(), "Crédito Vivienda");
        Assertions.assertEquals(Optional.of(credit).get().getAssociateId(), 1);
        Assertions.assertEquals(Optional.of(credit).get().getStatus(), true);
    }

    @Test
    void findByAssociateId() {
        int id = 1;
        Mockito.when(creditRepository.findByAssociate_Id(id)).thenReturn(List.of(credit));
        Assertions.assertNotNull(creditService.findByAssociateId(id));
        Assertions.assertEquals(List.of(credit).get(0).getId(), credit.getId());
        Assertions.assertEquals(List.of(credit).get(0).getDescription(), "Crédito Vivienda");
        Assertions.assertEquals(List.of(credit).get(0).getAssociateId(), id);
        Assertions.assertEquals(List.of(credit).get(0).getStatus(), true);
    }

    @Test
    void findByStatus() {
        Boolean status = true;
        Mockito.when(creditRepository.findByStatus(status)).thenReturn(List.of(credit));
        Assertions.assertNotNull(creditService.findByStatus(status));
        Assertions.assertEquals(List.of(credit).get(0).getId(), credit.getId());
        Assertions.assertEquals(List.of(credit).get(0).getDescription(), "Crédito Vivienda");
        Assertions.assertEquals(List.of(credit).get(0).getAssociateId(), 1);
        Assertions.assertEquals(List.of(credit).get(0).getStatus(), status);
    }
}