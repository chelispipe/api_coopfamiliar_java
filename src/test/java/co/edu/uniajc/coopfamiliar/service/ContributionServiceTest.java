package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.AssociateModel;
import co.edu.uniajc.coopfamiliar.model.ContributionModel;
import co.edu.uniajc.coopfamiliar.model.RolModel;
import co.edu.uniajc.coopfamiliar.repository.ContributionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

class ContributionServiceTest {

    @Mock
    private ContributionRepository contributionRepository;

    @InjectMocks
    private ContributionService contributionService;

    private ContributionModel contribution;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        String dateStr = "2021-09-10";
        Date dateIn = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);

        contribution =  new ContributionModel();
        contribution.setId(1);
        contribution.setDescription("Pago 1");
        contribution.setStatus(true);
        contribution.setAssociateId(1);
        contribution.setDueDay(25);
        contribution.setRefundDate(dateIn);
        contribution.setValue(25000.00);
    }

    @Test
    void createContribution() {
        Mockito.when(contributionRepository.save(contribution)).thenReturn(contribution);
        Assertions.assertNotNull(contributionService.createContribution(contribution));
        Assertions.assertEquals(contribution, contributionService.createContribution(contribution));
    }

    @Test
    void updateContribution() {
        Mockito.when(contributionRepository.save(contribution)).thenReturn(contribution);
        Assertions.assertNotNull(contributionService.updateContribution(contribution));
        Assertions.assertEquals(contribution, contributionService.updateContribution(contribution));
    }

    @Test
    void deleteContribution() {
        int id = 1;
        contributionService.deleteContribution(id);
        Mockito.verify(contributionRepository, Mockito.times(id)).deleteById(id);
    }

    @Test
    void findAllContribution() {
        Mockito.when(contributionRepository.findAll()).thenReturn(List.of(contribution));
        Assertions.assertNotNull(contributionService.findAllContribution());
        Assertions.assertTrue(List.of(contribution).size() > 0);
        Assertions.assertEquals(List.of(contribution).get(0).getDescription(), "Pago 1");
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(contributionRepository.findById(id)).thenReturn(Optional.of(contribution));
        Assertions.assertNotNull(contributionService.findById(id));
        Assertions.assertEquals(Optional.of(contribution).get().getId(), id);
        Assertions.assertEquals(Optional.of(contribution).get().getDescription(), "Pago 1");
        Assertions.assertEquals(Optional.of(contribution).get().getAssociateId(), 1);
        Assertions.assertEquals(Optional.of(contribution).get().getStatus(), true);
    }

    @Test
    void findBetweenDates() {
        String dateFrom = "2021-05-29", dateTo = "2021-09-10";
        Mockito.when(contributionRepository.findDataByDates(dateFrom, dateTo)).thenReturn(List.of(contribution));
        Assertions.assertNotNull(contributionService.findBetweenDates(dateFrom, dateTo));
        Assertions.assertTrue(List.of(contribution).size() > 0);
        Assertions.assertEquals(List.of(contribution).get(0).getDescription(), "Pago 1");
    }
}