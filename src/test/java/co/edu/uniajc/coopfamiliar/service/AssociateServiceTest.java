package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.AssociateModel;
import co.edu.uniajc.coopfamiliar.model.RolModel;
import co.edu.uniajc.coopfamiliar.repository.AssociateRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.*;


class AssociateServiceTest {

    @Mock
    private AssociateRepository associateRepository;

    @InjectMocks
    private AssociateService associateService;

    private AssociateModel associate;
    private List<RolModel> roles;
    private RolModel rol;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        roles = new ArrayList<>();
        rol = new RolModel(1, "Testing", "Rol de Pruebas", true);
        roles.add(rol);

        String dateStr = "2021-09-10";
        Date dateIn = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);

        associate = new AssociateModel();
        associate.setId(1);
        associate.setIdentificationNumber("123456");
        associate.setFirstName("Felipe");
        associate.setLastName("Medel");
        associate.setStatus(true);
        associate.setDateIn(dateIn);
        associate.setRoles(roles);

    }

    @Test
    void createAssociate() {
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
        Assertions.assertNotNull(associateService.createAssociate(associate));
        Assertions.assertEquals(associate, associateService.createAssociate(associate));
    }

    @Test
    void updateAssociate() {
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
        Assertions.assertNotNull(associateService.updateAssociate(associate));
        Assertions.assertEquals(associate, associateService.updateAssociate(associate));
    }

    @Test
    void deleteAssociate() {
        int id = 1;
        associateService.deleteAssociate(id);
        Mockito.verify(associateRepository, Mockito.times(id)).deleteById(id);
    }

    @Test
    void findAllAssociate() {
        Mockito.when(associateRepository.findAll()).thenReturn(List.of(associate));
        Assertions.assertNotNull(associateService.findAllAssociate());
        Assertions.assertTrue(List.of(associate).size() > 0);
        Assertions.assertEquals(List.of(associate).get(0).getFirstName(), "Felipe");
    }

    @Test
    void findAllByName() {
        String name = "Felipe";
        Mockito.when(associateRepository.findByFirstNameAndLastName(name)).thenReturn(List.of(associate));
        Assertions.assertNotNull(associateService.findAllByName(name));
        Assertions.assertEquals(List.of(associate).get(0).getFirstName(), "Felipe");
    }

    @Test
    void findAllByStatus() {
        Mockito.when(associateRepository.findByStatus(true)).thenReturn(List.of(associate));
        Assertions.assertNotNull(associateService.findAllByStatus(true));
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(associateRepository.findById(id)).thenReturn(Optional.of(associate));
        Assertions.assertNotNull(associateService.findById(id));
        Assertions.assertEquals(Optional.of(associate).get().getId(), id);
        Assertions.assertEquals(Optional.of(associate).get().getFirstName(), "Felipe");
        Assertions.assertEquals(Optional.of(associate).get().getLastName(), "Medel");
        Assertions.assertEquals(Optional.of(associate).get().getStatus(), true);
        Assertions.assertEquals(Optional.of(associate).get().getIdentificationNumber(), "123456");
    }

    @Test
    void findByIdentificationNumber() {
        String identificationNumber = "123456";
        Mockito.when(associateRepository.findAllByIdentificationNumber(identificationNumber))
                .thenReturn(List.of(associate));
        Assertions.assertNotNull(associateService.findByIdentificationNumber(identificationNumber));
        Assertions.assertEquals(List.of(associate).get(0).getId(), associate.getId());
        Assertions.assertEquals(List.of(associate).get(0).getFirstName(), "Felipe");
        Assertions.assertEquals(List.of(associate).get(0).getLastName(), "Medel");
        Assertions.assertEquals(List.of(associate).get(0).getStatus(), true);
    }
}