package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.FundModel;
import co.edu.uniajc.coopfamiliar.repository.FundRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FundServiceTest {

    @Mock
    private FundRepository fundRepository;
    
    @InjectMocks
    private FundService fundService;
    
    private FundModel fund;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        String dateStr = "2021-09-10";
        Date dateCreation = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        
        fund = new FundModel();
        fund.setId(1);
        fund.setContributionId(1);
        fund.setName("Fondo de Ahorro");
        fund.setStatus(true);
        fund.setDateCreation(dateCreation);
        fund.setValue(25000000.0);
        fund.setTotal(25000000.0);
    }

    @Test
    void createFund() {
        Mockito.when(fundRepository.save(fund)).thenReturn(fund);
        Assertions.assertNotNull(fundService.createFund(fund));
        Assertions.assertEquals(fund, fundService.createFund(fund));
    }

    @Test
    void updateFund() {
        Mockito.when(fundRepository.save(fund)).thenReturn(fund);
        Assertions.assertNotNull(fundService.updateFund(fund));
        Assertions.assertEquals(fund, fundService.updateFund(fund));
    }

    @Test
    void deleteFund() {
        int id = 1;
        fundService.deleteFund(id);
        Mockito.verify(fundRepository, Mockito.times(id)).deleteById(id);
    }

    @Test
    void findAllFund() {
        Mockito.when(fundRepository.findAll()).thenReturn(List.of(fund));
        Assertions.assertNotNull(fundService.findAllFund());
        Assertions.assertTrue(List.of(fund).size() > 0);
        Assertions.assertEquals(List.of(fund).get(0).getName(), "Fondo de Ahorro");
    }

    @Test
    void findByName() {
        String name = "Fondo de Ahorro";
        Mockito.when(fundRepository.findByName(name)).thenReturn(List.of(fund));
        Assertions.assertNotNull(fundService.findByName(name));
        Assertions.assertEquals(List.of(fund).get(0).getId(), fund.getId());
        Assertions.assertEquals(List.of(fund).get(0).getName(), "Fondo de Ahorro");
        Assertions.assertEquals(List.of(fund).get(0).getContributionId(), fund.getContributionId());
        Assertions.assertEquals(List.of(fund).get(0).getStatus(), true);
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(fundRepository.findById(id)).thenReturn(Optional.of(fund));
        Assertions.assertNotNull(fundService.findById(id));
        Assertions.assertEquals(Optional.of(fund).get().getId(), id);
        Assertions.assertEquals(Optional.of(fund).get().getName(), "Fondo de Ahorro");
        Assertions.assertEquals(Optional.of(fund).get().getContributionId(), fund.getContributionId());
        Assertions.assertEquals(Optional.of(fund).get().getStatus(), true);
    }
}