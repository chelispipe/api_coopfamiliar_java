package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.RolModel;
import co.edu.uniajc.coopfamiliar.repository.RolRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;


class RolServiceTest {

    @Mock
    private RolRepository rolRepository;

    @InjectMocks
    private RolService rolService;

    private RolModel rol;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        // Rol Test
        rol = new RolModel(1, "Testing", "Rol de Pruebas", true);
    }

    @Test
    void findAllRol() {
        Mockito.when(rolRepository.findAll()).thenReturn(List.of(rol));
        Assertions.assertNotNull(rolService.findAllRol());
        Assertions.assertTrue(List.of(rol).size() > 0);
        Assertions.assertEquals(List.of(rol).get(0).getName(), "Testing");
    }

    @Test
    void findAllByName() {
        String name = "admin";
        Mockito.when(rolRepository.findAllByName(name)).thenReturn(List.of(rol));
        Assertions.assertNotNull(rolService.findAllByName(name));
    }

    @Test
    void findAllByStatus() {
        Mockito.when(rolRepository.findByStatus(true)).thenReturn(List.of(rol));
        Assertions.assertNotNull(rolService.findAllByStatus(true));
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(rolRepository.findById(id)).thenReturn(Optional.of(rol));
        Assertions.assertNotNull(rolService.findById(id));
        Assertions.assertEquals(Optional.of(rol).get().getId(), id);
        Assertions.assertEquals(Optional.of(rol).get().getName(), "Testing");
        Assertions.assertEquals(Optional.of(rol).get().getDescription(), "Rol de Pruebas");
        Assertions.assertEquals(Optional.of(rol).get().getStatus(), true);
    }

    @Test
    void createRol() {
        Mockito.when(rolRepository.save(rol)).thenReturn(rol);
        Assertions.assertNotNull(rolService.createRol(rol));
        Assertions.assertEquals(rol, rolService.createRol(rol));
    }

    @Test
    void updateRol() {
        Mockito.when(rolRepository.save(rol)).thenReturn(rol);
        Assertions.assertNotNull(rolService.updateRol(rol));
        Assertions.assertEquals(rol, rolService.updateRol(rol));
    }

    @Test
    void deleteRol() {
        int id = 1;
        rolService.deleteRol(id);
        Mockito.verify(rolRepository, Mockito.times(id)).deleteById(id);

    }


}