package co.edu.uniajc.coopfamiliar.service;

import co.edu.uniajc.coopfamiliar.model.HelpModel;
import co.edu.uniajc.coopfamiliar.repository.HelpRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class HelpServiceTest {

    @Mock
    private HelpRepository helpRepository;

    @InjectMocks
    private HelpService helpService;

    private HelpModel help;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        String dateStr = "2021-09-10";
        Date dateCreation = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);

        help = new HelpModel();
        help.setId(1);
        help.setAssociateId(1);
        help.setFundId(1);
        help.setDateCreation(dateCreation);
        help.setType("Salud");
        help.setValue(250000.0);

    }

    @Test
    void createHelp() {
        Mockito.when(helpRepository.save(help)).thenReturn(help);
        Assertions.assertNotNull(helpService.createHelp(help));
        Assertions.assertEquals(help, helpService.createHelp(help));
    }

    @Test
    void updateHelp() {
        Mockito.when(helpRepository.save(help)).thenReturn(help);
        Assertions.assertNotNull(helpService.updateHelp(help));
        Assertions.assertEquals(help, helpService.updateHelp(help));
    }

    @Test
    void deleteHelp() {
        int id = 1;
        helpService.deleteHelp(id);
        Mockito.verify(helpRepository, Mockito.times(id)).deleteById(id);
    }

    @Test
    void findAllHelp() {
        Mockito.when(helpRepository.findAll()).thenReturn(List.of(help));
        Assertions.assertNotNull(helpService.findAllHelp());
        Assertions.assertTrue(List.of(help).size() > 0);
        Assertions.assertEquals(List.of(help).get(0).getType(), "Salud");
    }

    @Test
    void findByType() {
        String type = "Salud";
        Mockito.when(helpRepository.findByType(type)).thenReturn(List.of(help));
        Assertions.assertNotNull(helpService.findByType(type));
        Assertions.assertEquals(List.of(help).get(0).getId(), help.getId());
        Assertions.assertEquals(List.of(help).get(0).getType(), "Salud");
        Assertions.assertEquals(List.of(help).get(0).getAssociateId(), help.getAssociateId());
        Assertions.assertEquals(List.of(help).get(0).getFundId(), help.getFundId());
    }

    @Test
    void findById() {
        int id = 1;
        Mockito.when(helpRepository.findById(id)).thenReturn(Optional.of(help));
        Assertions.assertNotNull(helpService.findById(id));
        Assertions.assertEquals(Optional.of(help).get().getId(), id);
        Assertions.assertEquals(Optional.of(help).get().getType(), "Salud");
        Assertions.assertEquals(Optional.of(help).get().getAssociateId(), help.getAssociateId());
        Assertions.assertEquals(Optional.of(help).get().getFundId(), help.getFundId());
    }
}